//функции для главной страницы
// получаем данные первой страницы
async function getDataNavigation(i = 0) {
    let data = await fetch('https://gorest.co.in/public-api/posts?page' + '=' + i); 
    console.log(i);
    let treatment = await data.json();
    console.log(treatment);
    return treatment;
}


// формирую скелет списка
function formList(text,id) {
    let ol = document.querySelector('ol');

    let link = document.createElement('a');
    let li = document.createElement('li');

    link.classList.add('link-secondary', 'text-secondary');
    li.classList.add('list-group-item');

    link.href = 'article.html' + '?id=' + id;
    li.innerHTML = text;

    link.append(li);
    if (!ol) {
        
    }else{
        ol.append(link);
    };

    return li;
}


// функция присваивания значений и вырисовка списка
async function getListNavigation(i = 0) {
    let arr = await getDataNavigation(i);
    //clearList();
    console.log(i);
    console.log(arr);
    for (let k = 0; k < arr.data.length; k++) {
        let text = arr.data[k].title;
        let id = arr.data[k].id;
        formList(text, id);
    }
}

// формируем скелет навигации
function createFormNavigation(i = 1) {
    if (i === 9) {
        let pagination = document.querySelector('.page-item-btn');

        let pageItem = document.createElement('li');
        let pageLink = document.createElement('span');
    
        pageItem.classList.add('page-item-load', 'page-link');
        pageLink.classList.add('page-link1');
    
        //pageLink.href = 'main.html' + '?page=' + i;
        pageLink.innerHTML = 'Следующая';
    
        pageItem.append(pageLink);
        pagination.append(pageItem);
        return;
    }

    let pagination = document.querySelector('.page-item-btn');

    let pageItem = document.createElement('li');
    let pageLink = document.createElement('a');

    pageItem.classList.add('page-item');
    pageLink.classList.add('page-link', 'linkBtn');

    //pageLink.href = 'main.html' + '?page=' + i;
    pageLink.innerHTML = i;

    pageItem.append(pageLink);
    pagination.before(pageItem);

};

// присваем скелету навигации номера
function getNavigation(arr) {
    let pages = arr.meta.pagination.pages;
    for (let i = 1; i < pages; i++) {
        if ((i <= 8) || (i == pages-1) || i == 9) {
            createFormNavigation(i);
        };
    };
};

// функция для прокрутки нумерации страниц вперед
function btnLoadForward(arr) {
    let pages = arr.meta.pagination.pages;
    let btnLoad = document.querySelector('.page-item-load');
    let pageLink = document.querySelectorAll('.linkBtn');
    console.log(pages);
    btnLoad.addEventListener('click', () => {
        for (let i = 0; i < pageLink.length -1; i++) {
            if (Number(pageLink[i].innerHTML) == pages-8) {
                return;
            };
            pageLink[i].innerHTML = Number(pageLink[i].innerHTML) + 8;
            pageLink.href = 'main.html' + '?page=' + pageLink[i].innerHTML;
        };
    });
};

// функция для прокрутки нумерации страниц назад
function btnLoadBack(arr) {
    let pages = arr.meta.pagination.pages;
    let btnLoad = document.querySelector('.page-item-load2');
    let pageLink = document.querySelectorAll('.linkBtn');
    console.log(pages);
    btnLoad.addEventListener('click', () => {
        for (let i = 0; i < pageLink.length -1; i++) {
            if (Number(pageLink[i].innerHTML) < 8) {
                return;
            };
            pageLink[i].innerHTML = Number(pageLink[i].innerHTML) - 8;
            pageLink.href = 'main.html' + '?page=' + pageLink[i].innerHTML;
        };
    });
};

// функция объеденяющая несколько функций

async function combiningSeveralFunctions() {
    let arr = await getDataNavigation();
    console.log(arr);
    getListNavigation(i = 0, arr);
    getNavigation(arr);
    btnLoadForward(arr);
    btn();
    btnLoadBack(arr);
};

//функция загрузка списка при нажатии кнопки

function btn() {
    let pag = document.querySelector('.pagination');
    pag.addEventListener('click', event => { 
        if (event.target.classList.contains('linkBtn')) { 
            let i = Number(event.target.innerHTML);
             clearList();//работает
             createDiv();
             getListNavigation(i);
        }
      });
};



//функция по очистке списка статей
function clearList() {
    let a = document.querySelector('.list1').remove();
}

//функция по созданию скелета для списка статей
function createDiv() {
    let divHeader = document.querySelector('.header');

    let list1 = document.createElement('div');
    let ol = document.createElement('ol');

    list1.classList.add('list1');
    ol.classList.add('list-group', 'list-group-numbered');

    list1.append(ol);
    divHeader.after(list1);
}



//ФУНКЦИИ ДЛЯ ARTICLE
//получаем данные для страницы
async function getUrl() {
    let url= window.location.search;
    url = url.substr(4, 4);
    console.log(url);
    const data = await fetch('https://gorest.co.in/public-api/posts/' + url);
    const dataComment = await fetch('https://gorest.co.in/public-api/comments?post_id=' + url);
    const treatment = await data.json();
    const resp = await dataComment.json();
    console.log(treatment);
    console.log(resp);
    formArticle(treatment);
    getComments(resp);
};

//формируем скелет страницы

async function formArticle(treatment, resp) {
    let header = document.querySelector('.header__text');
    let text = document.querySelector('.text__article');

    header.innerHTML = await treatment.data.title;
    text.innerHTML = await treatment.data.body;
};

//функция для получения комментария
function getComments(resp) {
    for (let i = 0; i < resp.data.length; i++) {
        formComment(resp, i);
    }
}

// функция создания формы комментариев
function formComment(resp, i) {
    let textContainer = document.querySelector('.text__container')

    let div = document.createElement('div');
    let h3 = document.createElement('h3');
    let h4 = document.createElement('h4');
    let span = document.createElement('span');

    div.classList.add('textComment__container');
    h3.classList.add('name__author');
    h4.classList.add('email');
    span.classList.add('text__comment');

    h3.innerHTML = resp.data[i].name;
    h4.innerHTML = "email: " + resp.data[i].email;
    span.innerHTML = "'" + resp.data[i].body + "'";
    
    div.append(h3);
    div.append(h3);
    div.append(h4);
    div.append(span);
    textContainer.after(div);
};


























/*async function getData() {
    const data = await fetch('https://gorest.co.in/public-api/posts?page');
    const treatment = await data.json();
    //console.log(treatment);
    return treatment;
}*/


/*async function getList() {
    const arr = await getData();
    for (let i = 0; i < arr.data.length; i++) {
        let text = await arr.data[i].title;
        formList(text, i);
    }
}*/


/*function formList(text,k) {
    let ol = document.querySelector('ol');

    let link = document.createElement('a');
    let li = document.createElement('li');

    link.classList.add('link-secondary', 'text-secondary');
    li.classList.add('list-group-item');

    link.href = 'article.html' + '?page=' + k;
    li.innerHTML = text;

    link.append(li);
    if (!ol) {
        
    }else{
        ol.append(link);
    };

    return li;
}

async function getUrl() {
    let url= window.location.search;
    url = url.substr(6, 1);
    //console.log(url);
    let arr = await getDataNavigation();
    for (let i = 0; i < arr.data.length; i++) {
       if (url == i) {
        formArticle(arr, i);
       };
    };
};

async function formArticle(arr, i) {
    let header = document.querySelector('.header__text');
    let text = document.querySelector('.text__article');

    header.innerHTML = await arr.data[i].title;
    text.innerHTML = await arr.data[i].body;
};

// 1.создать форму навигации. 2. присовить каждой форме ссылку на страницу.3.

async function getListNavigation(i = 0) {
    let arr = await getDataNavigation(i);
    console.log(i);
    for (let k = 0; k < arr.data.length; k++) {
        let text = await arr.data[k].title;
        formList(text, k);
    }
}

async function getDataNavigation(i = 0) {
    console.log(i);
    let data = await fetch('https://gorest.co.in/public-api/posts?page' + '=' + i); // нужно удалить ноу корс
    let treatment = await data.json();
    console.log(treatment);
    return treatment;
}

async function getNavigation() {
    //const arr = await getData();
    let arr = await getDataNavigation();
    //console.log(arr.meta.pagination.pages);
    let pages = arr.meta.pagination.pages;
    for (let i = 1; i < pages; i++) {
        if ((i <= (pages-pages + 8)) || (i == pages-1) || i == (pages-pages + 9)) {
            createFormNavigation(i);
        };
    };
    btnLoad(); // загрузили фунцию для btn load, но на первой странице остальная нумерация не обладает функцией
    asignBtn(); // здесь функция будет работать с начальными параметрами
};

function clearList() {
    let a = document.querySelector('.list1').remove();
}

function createDiv() {
    let divHeader = document.querySelector('.header');

    let list1 = document.createElement('div');
    let ol = document.createElement('ol');

    list1.classList.add('list1');
    ol.classList.add('list-group', 'list-group-numbered');

    list1.append(ol);
    divHeader.after(list1);
}

function createFormNavigation(i = 1) {
    if (i === 9) {
        let pagination = document.querySelector('.page-item-btn');

        let pageItem = document.createElement('li');
        let pageLink = document.createElement('span');
    
        pageItem.classList.add('page-item-load', 'disabled');
        pageLink.classList.add('page-link1');
    
        //pageLink.href = 'main.html' + '?page=' + i;
        pageLink.innerHTML = '...';
    
        pageItem.append(pageLink);
        pagination.before(pageItem);
        return;
    }

    let pagination = document.querySelector('.page-item-btn');

    let pageItem = document.createElement('li');
    let pageLink = document.createElement('a');

    pageItem.classList.add('page-item');
    pageLink.classList.add('page-link', 'linkBtn');

    //pageLink.href = 'main.html' + '?page=' + i;
    pageLink.innerHTML = i;

    pageItem.append(pageLink);
    pagination.before(pageItem);

};

async function btnLoad() {
    let arr = await getDataNavigation();
    let btnLoad = document.querySelector('.page-item-load');
    let pageLink = document.querySelectorAll('.linkBtn');
    console.log(pageLink);
    btnLoad.addEventListener('click', () => {
        for (let i = 0; i < pageLink.length -1; i++) {
            pageLink[i].innerHTML = Number(pageLink[i].innerHTML) + 8;
        };
        asignBtn();
    })
    //asignBtn();
    /*console.log(arrI.length);
    btn(arrI.length);
}

function asignBtn() {
    let arrI = [];
    let pageLink = document.querySelectorAll('.linkBtn');
    for (let i = 0; i < pageLink.length -1; i++) { //изменил класс на linkBtn - нужно разобраться с количеством кнопок на страницы
        arrI.push(pageLink[i].innerHTML);
    };
    /*for (let a = arrI[0]; a <= arrI[arrI.length]; a++) {
        console.log(pageLink[a]);
        pageLink[a].href = 'main.html' + '?page=' + pageLink[a].innerHTML;
        console.log(pageLink[a]);
    }
    //console.log(arrI);
    btn(Number(arrI[0]), Number(arrI[7]));
}


async function btn(j = 1, f = 8) {
    let arr = await getDataNavigation();
    let btn = document.querySelectorAll('.linkBtn');
    //console.log(btn.length);
    for (let i = j; i <= f; i++) {
        btn[i-j].addEventListener('click', () => {
            //console.log(i);
            //console.log(j);
             clearList();
             createDiv();
             //let arrNew = getDataNavigation(i);
             //console.log(arrNew);
             getListNavigation(i);
        });
    }; 
};


/*async function btn() {
    const arr = await getData();
    let btn = document.querySelectorAll('.page-item');
    console.log(btn);
    for (let i = 0; i < btn.length ; i++) {
        btn[i].addEventListener('click', async () => {
             clearList();
             createDiv();
             let arrNew = getDataNavigation(i);
             console.log(arrNew);
             getListNavigation(i);
        })   
    }; 
};*/